

import java.util.Scanner;

/**
 * Converts occidental (i.e. Julian / Gregorian) dates to hebrew (or: jewish) dates
 * @author Melvin Tas, Jonas Tochtermann
 */
public class Main {

	public static void main(String[] args) {

		/**
		 * FIELDS
		 * hebCal: instance of HebrewCalendar
		 * input: basis for input possibility
		 * c: variable for option choosing
		 * validation: placeholder for input validation
		 */
		HebrewCalendar hebCal = HebrewCalendar.getInstance();
		Scanner input = new Scanner(System.in);
		char c = '9';
		boolean validation;

		hebCal.setToday(); // initial Date for the menu to convert / show

		do {
			TUI.menu(hebCal);
			validation = false;
			while (!validation) {
				c = input.nextLine().charAt(0);
				if (c == '1' || c == '0') {
					validation = true;
				} else {
					TUI.notAnOption();
				}
			}
			if (c == '1') {
				TUI.newDateCalculation(hebCal, input);
			}
		} while (c != '0');

		// final operations
		input.close();
		System.out.println("	Auf Wiedersehen!");
	}

}
