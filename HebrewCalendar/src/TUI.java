

import java.util.Scanner;

/**
 * Converts occidental (i.e. Julian / Gregorian) dates to hebrew (or: jewish) dates
 * @author Melvin Tas, Jonas Tochtermann
 */
public class TUI {

	/**
	 * shows main interface: title, date (weekday, occidental, hebrew), options
	 * @param hebCal: hebrewCalendar-instance from main
	 */
	static void menu(HebrewCalendar hebCal) {
		hebCal.occidentalToHebrew();
		String weekDay = hebCal.getWeekday();
		String occidentalDate = hebCal.getOccidentalDate();
		String hebrewDate = hebCal.getHebrewDate();

		System.out.println();
		System.out.println("*********************************");
		System.out.println("**** Kleiner Kalenderrechner ****");
		System.out.println("*********************************");
		System.out.println();
		System.out.println("	" + weekDay);
		System.out.println("	" + occidentalDate);
		System.out.println("	" + hebrewDate);
		System.out.println();
		System.out.println("	Wählen Sie eine Möglichkeit: ");
		System.out.println("	[1]  Datum umrechnen");
		System.out.println("	[0]  Programm beenden");
		System.out.println();
		System.out.print("	Wahl: ");

	}

	/**
	 * prints user correction
	 */
	static void notAnOption() {
		System.out.println();
		System.out.println("	Die gewählte Option existiert nicht!");
		System.out.println("	Bitte wählen Sie eine gültige Option");
		System.out.println();
		System.out.print("	Wahl: ");
	}

	/**
	 * calculates a new given date
	 * @param hebCal
	 * @param input
	 */
	static void newDateCalculation(HebrewCalendar hebCal, Scanner input) {
		String shortDate = hebCal.getShortDate();
		boolean validation = false;
		String dateString;
		int[] date = {0, 0, 0};

		System.out.println();
		System.out.println("	Geben Sie ein Datum zur Umrechung ein (z.B. " + shortDate + "):");
		System.out.print("	");
		while (!validation) {
			dateString = input.nextLine();
			date = dateValidation(dateString);
			if (date[0] != 0) {
				validation = true;
			} else {
				System.out.println("	Die Eingabe konnte leider nicht verarbeitet werden.");
				System.out.println("	Bitte versuchen Sie es noch einmal: ");
				System.out.print("	");
			}
		}
		hebCal.setDate(date[0], date[1], date[2]);
	}

	/**
	 * checks if input is a valid date
	 * @param dateString: given input
	 * @return: input transformed in an array with {day, month, year} (occidental)
	 */
		private static int[] dateValidation (String dateString){
			int[] date = {0, 0, 0}; // prepared return
			int maxDays = 0;

			// checks input if valid date
			if (!dateString.matches("(\\d+).(\\d+).-?(\\d+)")) {
				return date;
			}
			dateString = dateString.replace(".", " "); // whitespace is easier to handle than dots
			String[] dateStringArray = dateString.split("\\s");

			// transports String array to int array
			for (int i = 0; i < dateStringArray.length; i++) {
				date[i] = Integer.parseInt(dateStringArray[i]);
			}

			// checks day input according to month length
			switch (date[1]) {
				// january, march, may, july, august, october, december
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					maxDays = 31;
					break;

				// april, june, september, november
				case 4:
				case 6:
				case 9:
				case 11:
					maxDays = 30;
					break;

				// february
				case 2:
					if (occidentalLeap(date[2])) {
						maxDays = 29;
					} else {
						maxDays = 28;
					}
			}

			// checks month input
			if (date[1] < 1 || date[1] > 12 || date[0] < 0 || date[0] > maxDays) {
				date[0] = 0;
			}
			return date;
	}

	/**
	 * checks if given occidental year is a leap year
	 * @param year: given occidental year
	 * @return true if leap year
	 */
	private static boolean occidentalLeap(int year) {
		return year % 400 == 0 || (year % 100 != 0 && year % 4 == 0) || (year % 100 == 0 && year < 1582);
	}


}
