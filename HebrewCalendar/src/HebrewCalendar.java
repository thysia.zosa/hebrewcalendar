

import java.time.LocalDate;

/**
 * Singleton-patterned data class for occidental-to-hebrew calendar calculation
 * @author Melvin Tas, Jonas Tochtermann
 */
class HebrewCalendar {

	/**
	 * FIELDS
	 * occidentalDay: Day number of occidental date
	 * occidentalMonth: Month number of occidental Month (1-12)
	 * occidentalYear: Year number of occidental date
	 * julianDate: julian date of given occidental date
	 * occidentalDay: Day number of hebrew date
	 * occidentalMonth: INTERN Month number of hebrew Month (WATCH ARRAY hebrewMonths!)
	 * occidentalYear: Year number of hebrew date
	 * weekdayNumber: number of Day in days of week (Sunday to Saturday)
	 */
	private static int occidentalDay;
	private static int occidentalMonth;
	private static int occidentalYear;
	private static int julianDate;
	private static int hebrewDay;
	private static int hebrewMonth;
	private static int hebrewYear;
	private static int weekdayNumber;

	/**
	 * weekday Names for weekdayNumber
	 */
	private static String[] weekDays  = {
			"Sonntag",
			"Montag",
			"Dienstag",
			"Mittwoch",
			"Donnerstag",
			"Freitag",
			"Samstag"
	};

	/**
	 * hebrew Month names for hebrewMonth (non-standard numbering!)
	 */
	private static String[] hebrewMonths = {
			"Adar",
			"Nisan",
			"Iyyar",
			"Siwan",
			"Tammuz",
			"Av",
			"Elul",
			"Teschri",
			"Cheschwan",
			"Kislew",
			"Tevet",
			"Schvat",
			"Adar Rischon",
			"Adar Schni",
	};

	/**
	 * names of occidental Months
	 */
	private static String[] occidentalMonths = {
			"Januar",
			"Februar",
			"März",
			"April",
			"Mai",
			"Juni",
			"Juli",
			"August",
			"September",
			"Oktober",
			"November",
			"Dezember"
	};

	/**
	 * Singleton-Implementation
	 */
	private static HebrewCalendar instance;
	private HebrewCalendar() {}
	static HebrewCalendar getInstance() {
		if (instance == null) {
			instance = new HebrewCalendar();
		}
		return instance;
	}

	/**
	 * sets Today as "given date" to convert
	 */
	void setToday() {
		LocalDate ld = LocalDate.now();
		int day = ld.getDayOfMonth();
		int month = ld.getMonthValue();
		int year = ld.getYear();
		setDate(day, month, year);
	}

	/**
	 * sets a given date to occidental Date parameters
	 * @param day: number of day in month
	 * @param month: number of month in year
	 * @param year: number of year
	 */
	void setDate(int day, int month, int year) {
		occidentalDay = day;
		occidentalMonth = month;
		occidentalYear = year;
	}

	/**
	 * calculates julian date from occidental date parameters
	 */
	private void occidentalToJulianDate() {
		int monthNumber;
		int yearNumber;
		int julianDateNumber;
		
		monthNumber = occidentalMonth < 3 ? occidentalMonth + 9 : occidentalMonth - 3;
		yearNumber = monthNumber < 10 ? occidentalYear : occidentalYear - 1;
		julianDateNumber = (int) Math.floor(yearNumber * 365.25) + (int) Math.floor(monthNumber * 30.6 + 0.5) + occidentalDay + 1721117;
		julianDateNumber = julianDateNumber > 2299170 ? julianDateNumber + 2 + Math.floorDiv(yearNumber, 400) - Math.floorDiv(yearNumber, 100) : julianDateNumber;
		julianDate = julianDateNumber;
	}

	/**
	 * calculates the days from 1.1.1 to the day before 1.1 of a given year (all in hebrew calendar
	 * @param yearsNumber: hebrew year
	 * @return number of past days since 1.1.1 including
	 */
	private long roshHashanaEven(int yearsNumber) {
		long cyclesNumber;
		long cycleYearNumber;
		long cycleMonthNumber;
		long fullDayNumber;
		long chalaqimNumber;
		long weekDayNumber;
		
		yearsNumber--;
		cyclesNumber = Math.floorDiv(yearsNumber, 19);
		cycleYearNumber = Math.floorMod(yearsNumber, 19);
		cycleMonthNumber = 12 * cycleYearNumber + (int) Math.floor(0.37 * cycleYearNumber + 0.06);
		chalaqimNumber = cyclesNumber * 17875 + cycleMonthNumber * 13753 + 5604;
		fullDayNumber = cyclesNumber * 6939 + cycleMonthNumber * 29 + Math.floorDiv(chalaqimNumber, 25920);
		chalaqimNumber = Math.floorMod(chalaqimNumber, 25920);
		weekDayNumber = Math.floorMod(fullDayNumber, 7);

		if (isLeapYear(yearsNumber) && weekDayNumber == 0 && chalaqimNumber >= 16789) {
			chalaqimNumber = 19440;
		}
		
		if (!isLeapYear(yearsNumber + 1) && weekDayNumber == 1 && chalaqimNumber >= 9924) {
			chalaqimNumber = 19440;
		}
		
		if (chalaqimNumber >= 19440) {
			fullDayNumber++;
		}
		
		weekDayNumber = Math.floorMod(fullDayNumber, 7);
		if (weekDayNumber == 2 || weekDayNumber == 4 || weekDayNumber == 6) {
			fullDayNumber++;
		}
		
		return fullDayNumber;
	}

	/**
	 * checks if a given hebrew year is a leap year
	 * @param yearsNumber: given hebrew year
	 * @return true if leap year
	 */
	private boolean isLeapYear(int yearsNumber) {
		switch (Math.floorMod(yearsNumber - 1, 19) + 1) {
			case 3:
			case 6:
			case 8:
			case 11:
			case 14:
			case 17:
			case 19:
				return true;
			default:
				return false;
		}
	}

	/**
	 * calculates the hebrew date from a julian date
	 */
	private void julianDateToHebrew() {
		int year;
		int monthNumber;
		int dayNumber;
		long beforeNewYear;
		long hebrewEpochDate;
		int cyclesNumber;
		int thisYearsDay;
		int yearLength;

		hebrewEpochDate = julianDate - 347997;
		monthNumber = (int) Math.floor(hebrewEpochDate / 29.53059414);
		cyclesNumber = Math.floorDiv((monthNumber - 1), 235);
		monthNumber -= cyclesNumber * 235;
		year = cyclesNumber * 19;
		year += 1 + Math.floor((monthNumber + 0.94) / 12.36842105);
		
		beforeNewYear = roshHashanaEven(year);
		
		if (beforeNewYear >= hebrewEpochDate) {
			year--;
			beforeNewYear = roshHashanaEven(year);
		}
		thisYearsDay = (int) (hebrewEpochDate - beforeNewYear);
		
		yearLength = (int) (roshHashanaEven(year + 1) - beforeNewYear);
		dayNumber = Math.floorMod(thisYearsDay + 205, yearLength);
		
		if (dayNumber < 265) {
			dayNumber++;
			monthNumber = (int) Math.floor(dayNumber / 29.55);
			dayNumber -= (int) Math.floor(29.5 * monthNumber);
		} else {
			dayNumber -= 264;
			switch (yearLength) {
				case 353:
				case 383:
					monthNumber = (int) Math.floor((dayNumber + 0.15) / 29.55);
					dayNumber -= (int) Math.floor(29.4 * monthNumber);
					monthNumber += 9;
					break;
				case 354:
				case 384:
					monthNumber = (int) Math.floor((dayNumber - 0.95) / 29.52);
					dayNumber -= (int) Math.floor(29.5 * monthNumber + 0.9);
					monthNumber += 9;
					break;
				case 355:
				case 385:
					monthNumber = 1 + (int) Math.floor((dayNumber - 1.95) / 29.52);
					dayNumber += 29 - (int) Math.floor(29.6 * monthNumber + 0.9);
					monthNumber += 8;
			}
		}
		
		if (isLeapYear(year) && monthNumber == 0) {
			monthNumber = 13;
		}
		
		hebrewDay = dayNumber;
		hebrewMonth = monthNumber;
		hebrewYear = year;
	}

	/**
	 * calculates the weekday number from a julian date
	 */
	private void julianDateWeekday() {
		weekdayNumber = Math.floorMod(julianDate + 1, 7);
	}

	/**
	 * gets the Name to the calculated weekday Number
	 * @return name of weekday
	 */
	String getWeekday() {
		return weekDays[weekdayNumber];
	}

	/**
	 * gets String with hebrew date
	 * @return String with hebrew date
	 */
	String getHebrewDate() {
		String yearInCounting;
		if (hebrewYear < 1) {
			yearInCounting = Math.abs(hebrewYear - 1) + " v. d. Z.";
		} else {
			yearInCounting = hebrewYear + " d. Z.";
		}
		return hebrewDay
				+ ". " 
				+ hebrewMonths[hebrewMonth] 
				+ " " 
				+ yearInCounting;
	}

	/**
	 * gets String with occidental date
	 * @return String with occidental date
	 */
	String getOccidentalDate() {
		String yearInCounting;
		if (occidentalYear < 1) {
			yearInCounting = Math.abs(occidentalYear - 1) + " v. Chr.";
		} else {
			yearInCounting = occidentalYear + " n. Chr.";
		}
		return occidentalDay
				+ ". " 
				+ occidentalMonths[occidentalMonth - 1] 
				+ " " 
				+ yearInCounting;
	}

	/**
	 * gets String with short occidental date (e.g. 1.1.1999)
	 * @return String with short occidental date
	 */
	String getShortDate() {
		return occidentalDay
				+ "."
				+ occidentalMonth
				+ "."
				+ occidentalYear;
	}

	/**
	 * converts the occidental date parameters to hebrew date parameters via julian Date
	 */
	void occidentalToHebrew() {
		occidentalToJulianDate();
		julianDateWeekday();
		julianDateToHebrew();
	}

}
